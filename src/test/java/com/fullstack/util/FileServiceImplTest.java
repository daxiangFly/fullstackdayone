package com.fullstack.util;

import com.fullstack.util.FileService;
import com.fullstack.util.FileServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class FileServiceImplTest {

    private File testFile;

    @Before
    public void setUp() throws Exception {
         this.testFile = new File("./src/test/resources/test.txt");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testFileReading() {
        FileService fileService = new FileServiceImpl();
        String[] content = fileService.fileReading(this.testFile);
        String[] expected = {"111","222","333"};
        Assert.assertArrayEquals("file read",expected,content);
    }

    @Test
    public void testFileWriting() {
        FileService fileService = new FileServiceImpl();
        String[] content = {"111","222","333"};
        Integer index = fileService.fileWriting(this.testFile,content);
        Assert.assertEquals(3L,index.longValue());
    }
}