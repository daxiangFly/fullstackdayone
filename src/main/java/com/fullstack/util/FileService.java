package com.fullstack.util;

import java.io.File;

/**
 * @program: fullstackdayone
 * @Description:
 * @author: Mr.Cheng
 * @date: 2021/10/13 10:59 上午
 */
public interface FileService {
    
    /**
    * @Description: read file by line
    * @Param: [file]
    * @return: java.lang.String[]
    * @Author: Mr.Cheng
    * @Date: 2021/10/13 11:03 上午
    */ 
    String[] fileReading(File file);

    /**
    * @Description: write content to txt file
    * @Param: [file, content]
    * @return: java.lang.Integer
    * @Author: Mr.Cheng
    * @Date: 2021/10/13 11:33 上午
    */
    Integer fileWriting(File file, String[] contents);

}
