package com.fullstack.helloworld;

import com.fullstack.user.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @program: fullstackdayone
 * @Description:
 * @author: Mr.Cheng
 * @date: 2021/10/11 2:29 上午
 */
public class HelloWorld {

    private final static Logger logger = LoggerFactory.getLogger(HelloWorld.class);

    public static void main(String[] args) {

        ArrayList<Integer> temp = new ArrayList<>();
        Integer[] a = {1,2};
        Integer[] demos = Arrays.copyOf(temp.toArray(a),2);
        for(Object demo : demos){
            System.out.println(demo);
        }

        if(args.length > 0){
            System.out.println("Hello " + args[0]);
        }else {
            System.out.println("Hello World");
        }
        logger.trace(PinType.FORGET_PASSWORD.getMessage());
        logger.debug(PinType.UPDATE_PHONE_NUMBER.getMessage());
        logger.info("Hello World info");
        logger.warn("Hello World warn");
        logger.error("Hello World error");
    }
}
