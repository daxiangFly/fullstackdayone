package com.fullstack.helloworld;

/**
 * @program: fullstackdayone
 * @Description:
 * @author: Mr.Cheng
 * @date: 2021/10/14 11:55 上午
 */
public enum Season {
    SPRING(1), SUMMER(2), AUTUMN(3), WINTER(4);

    private int id;

    private Season(int id) {
        this.id = id;
    }
}
