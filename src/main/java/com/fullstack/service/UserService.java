package com.fullstack.service;

import com.fullstack.user.AbstractUser;

/**
 * @program: fullstackdayone
 * @Description:
 * @author: Mr.Cheng
 * @date: 2021/10/12 3:15 下午
 */
public interface UserService {

    /**
    * @Description: 用户登录
    * @Param: []
    * @return: int
    * @Author: Mr.Cheng
    * @Date: 2021/10/12 3:17 下午
    */
    Boolean login(AbstractUser user);
}
