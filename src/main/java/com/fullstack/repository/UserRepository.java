package com.fullstack.repository;

import com.fullstack.user.AbstractUser;

/**
 * @program: fullstackdayone
 * @Description:
 * @author: Mr.Cheng
 * @date: 2021/10/13 3:09 下午
 */
public interface UserRepository {
    /**
    * @Description: find user information by name
    * @Param: [name]
    * @return: com.fullstack.user.AbstractUser
    * @Author: Mr.Cheng
    * @Date: 2021/10/13 3:18 下午
    */ 
    AbstractUser findUserByname(AbstractUser user);
    
    /**
    * @Description: save user information
    * @Param: [user]
    * @return: java.lang.Boolean
    * @Author: Mr.Cheng
    * @Date: 2021/10/13 3:23 下午
    */ 
    Boolean saveUser(AbstractUser user);
}
